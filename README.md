## RC-Potato Scripts
Private RammyCraft Scripts

## Current Scripts
- RC-PotatoTown
- RC-PotatoLock
- RC-PotatoChat
- RC-PotatoEssentials
- RC-PotatoLogin
- RC-PotatoEconomy

## Features
- Chat System (Release)
- Essentials (Release)
- Login System (Release)
- Lock System (Release)
- Town System (Release)
- Economy System (Beta)

## Contributors

### Scripters

- TomYaMee
- Atsukio
- Scorpix

### Bug Catchers
![alt text](https://minotar.net/helm/jjlau/36.png "jjlau")
![alt text](https://minotar.net/helm/CLEARITZ/36.png "CLEARITZ")
![alt text](https://minotar.net/helm/RaymondLee/36.png "RaymondLee")
![alt text](https://minotar.net/helm/MilkTeaaaaa/36.png "MilkTeaaaaa")
![alt text](https://minotar.net/helm/cwkitkat/36.png "cwkitkat")
![alt text](https://minotar.net/helm/issameZarone/36.png "issameZarone")
![alt text](https://minotar.net/helm/RedDragon/36.png "RedDragon")
![alt text](https://minotar.net/helm/Eddayy/36.png "Eddayy")
![alt text](https://minotar.net/helm/bottomfeeder/36.png "bottomfeeder")

## Links
- [RammyCraft Website](https://www.rammycraft.pw "RammyCraft Website")
- [RammyCraft Official Gaming Fan Page](https://www.facebook.com/TeamPotatoes "RammyCraft Official Gaming Fan Page") 
- [RammyCraft Discussion Group](https://www.facebook.com/groups/RammyCraft "RammyCraft Discussion Group")