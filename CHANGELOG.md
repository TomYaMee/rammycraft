# Changelog

## [v1.16.1-1](https://gitlab.com/TomYaMee/rammycraft/-/tags/1.16.1-1)

### Fix
#### PotatoLock
- Fix normal shulkerbox not restoring from explosion [Thanks MilkTeaaaaa]
- Storage blocks (Shulkerbox, furnace, dropper, dispenser) will now restore to their original direction upon explosion restoration (Fixes #54)
- Fix chest restoration not restoring tools' data (enchantments, durability, etc) (Fixes #79) [Thanks bottomfeeder]
- Fix link not opening correct link
- Send PvP disabled message as action bar to declutter the chat
- AutoAFK detection is now increased to 5 minutes, with the auto AFK detection fixed, the timer is now increased to lessen the spam
- Implement town rename, 100000 RammyCoins per rename, free for first time

## [v1.16.1-0](https://gitlab.com/TomYaMee/rammycraft/-/tags/1.16.1-0)

### New 
- Implement protection for name tagged mob from outsider
- Prevent portal creation if the portal teleportation is attempting to create a portal in a town not belong to the player. - under build flag
- Implement wither explosion protection - under explosion flag - Town
- Implement protection against anvil, sweet berry bush, composter - under interact flag
- Prevent outsider from teleporting into town with chorus fruit - under interact flag
- Decluttered system messages for Potato Town, event messages are now sent through action bar instead of spamming the chat.
- Rebuild RammyLogin, prevent same name from joining, prevent illegal character names from joining
  
### Fix
#### PotatoEssentials
- Fix majority sleep not excluding exploreworld (Already live)
- Fix console spam when player disconnect, re-optimize tab list update to 5 seconds from 1 second
- Improve auto AFK detection, it should work more reliably now hopefully its fully fixed

#### PotatoTown
- Fix non player entities not entering vehicles (Already live)
- Fix item frame being able to place anywhere (Already live)
- Fix town list owner always show as online
- Fix ignition flag not preventing lightning from transforming pigs and villagers. Bukkit API doesn't seem to support mooshroom transform.
- Fix outsider can interact with bone meal
- Fix outsider being able to place boats or minecart - under build flag
- Fix outsider able to steal water mobs using water bucket - under interact flag
- Fix outsider being able to breed mobs - under interact flag
- Fix bell interaction for outsider - under interact flag
- Fix PR can ride mobs and interact with armor stand - under interact flag
- Fix toggleable blocks not protected - pressure plate, trapdoor, redstone items, lever, button, tripwire - under interact flag
- Prevent lectern book from being taken - under interact flag
- Fix outsiders able to break bed if part of the bed is outside the protected chunk - under break flag
- Fix outsider from breaking liypads with boat - under break flag
- Fix armor stand not protected from outsider - under break flag
- Fix eating issue in Safezone

#### PotatoLock
- Implement protection against hopper minecart

#### PotatoChat
- Fix link in chat

### Rework
- Remove auto restart script, handled through hoster now
- Remove outdated plugin lists
- Decoupled regex name check from TuSKe, use Java built in regex instead
- Decommission json.sk, use built in instead